package cz.muni.ics.edirex.cbioondemandK8S.model;

public class Instance {
    private String id;
    private User user;
    private long secondsToExpire;
    private String url;
    private Status status;

    public Instance(String id, User user, long secondsToExpire, String url, Status status) {
        this.id = id;
        this.user = user;
        this.secondsToExpire = secondsToExpire;
        this.url = url;
        this.status = status;
    }

    public long getSecondsToExpire() {
        return secondsToExpire;
    }

    public void setSecondsToExpire(long secondsToExpire) {
        this.secondsToExpire = secondsToExpire;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public void fillReport(long timeToExpire, Status status){
        secondsToExpire = timeToExpire;
        this.status = status;
    }
}
