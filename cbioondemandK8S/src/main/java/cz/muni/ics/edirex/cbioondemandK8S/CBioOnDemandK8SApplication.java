package cz.muni.ics.edirex.cbioondemandK8S;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class CBioOnDemandK8SApplication {
	public static void main(String[] args) {
		SpringApplication.run(CBioOnDemandK8SApplication.class, args);
	}
}
