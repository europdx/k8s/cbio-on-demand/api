package cz.muni.ics.edirex.cbioondemandK8S.model;

import java.util.Objects;

public class InstanceId {
    private String id;
    private User user;

    public InstanceId(String id, User user) {
        this.id = id;
        this.user = user;
    }

    public InstanceId() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InstanceId that = (InstanceId) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getUser(), that.getUser());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getUser());
    }

    public Instance getInstance(){
        return new Instance(id, user, 0 , null , null);
    }
}
