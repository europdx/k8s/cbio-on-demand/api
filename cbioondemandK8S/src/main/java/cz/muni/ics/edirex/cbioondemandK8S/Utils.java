package cz.muni.ics.edirex.cbioondemandK8S;

import com.cronutils.builder.CronBuilder;
import com.cronutils.model.Cron;
import com.cronutils.model.CronType;
import com.cronutils.model.definition.CronDefinitionBuilder;
import com.cronutils.model.time.ExecutionTime;
import com.cronutils.parser.CronParser;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import cz.muni.ics.edirex.cbioondemandK8S.model.IdentifierCRD;
import io.kubernetes.client.JSON;
import io.kubernetes.client.util.Yaml;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.time.ZonedDateTime;
import java.util.Date;

import static com.cronutils.model.field.expression.FieldExpressionFactory.always;
import static com.cronutils.model.field.expression.FieldExpressionFactory.on;

/**
 * DO NOT USE IN PRODUCTION
 */
@Component
public class Utils {
    public static final long ONE_MINUTE_IN_MILLIS = 60000;
    public static final long HOUR = ONE_MINUTE_IN_MILLIS * 60;
    public static final long FIVE_MINUTES = 60 * 5;
    private static Gson gson = new Gson();


    private static InputStream getFileFromResource(String fileName){
        ClassLoader classLoader = Utils.class.getClassLoader();

        return classLoader.getResourceAsStream(fileName);
    }

    public static Object getConfigurationFromYaml(String fileName) throws IOException {
        return Yaml.load(new InputStreamReader(getFileFromResource(fileName)));
    }

    public static IdentifierCRD getIdentifierFromYaml(String fileName) throws IOException {
        Yaml.addModelMap("example.com/v1beta1", "identifier", IdentifierCRD.class);
        return Yaml.loadAs(new InputStreamReader(getFileFromResource(fileName)), IdentifierCRD.class);
    }

    public  static  String getCronExprresionFromDate(Date newDate){
        Cron cron = CronBuilder.cron(CronDefinitionBuilder.instanceDefinitionFor(CronType.UNIX))
                .withDoM(always())
                .withMonth(always())
                .withDoW(always())
                .withHour(on(newDate.getHours()))
                .withMinute(on(newDate.getMinutes()))
                .instance();
        return cron.asString();
    }

    public static long getExpireTimeFromCronExpresion(String cronTime){
        CronParser parser = new CronParser(CronDefinitionBuilder.instanceDefinitionFor(CronType.UNIX));
        Cron cron = parser.parse(cronTime);
        ExecutionTime executionTime = ExecutionTime.forCron(cron);
        ZonedDateTime now = ZonedDateTime.now();
        return executionTime.timeToNextExecution(now).get().getSeconds();
    }

    public static IdentifierCRD convertObjectToIdentifier(Object o) {
        if (o == null) return null;
        Gson gson = new JSON().getGson();
        JsonElement jsonElement = gson.toJsonTree(o);
        return gson.fromJson(jsonElement, IdentifierCRD.class);
    }

    public static  <T> T deepCopy(T object, Class<T> type) {
        return gson.fromJson(gson.toJson(object, type), type);
    }
}
