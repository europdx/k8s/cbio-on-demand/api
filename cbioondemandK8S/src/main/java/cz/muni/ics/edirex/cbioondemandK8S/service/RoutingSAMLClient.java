package cz.muni.ics.edirex.cbioondemandK8S.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

@Service
public class RoutingSAMLClient implements RoutingAAI{

    private RestTemplate rest;

    @Value("${proxy.api.url:http://cbio-proxy-api}")
    private String FQDN;
    private Logger logger = LoggerFactory.getLogger(RoutingSAMLClient.class);


    public RoutingSAMLClient(){
        this.rest = new RestTemplate();
    }

    @Override
    public boolean register(String url, String serviceName) {
        try {
            ResponseEntity<?> response = rest.exchange(FQDN + "/routes?sp=" + url + "&sf=" + serviceName,
                    HttpMethod.POST,null, Object.class, Collections.EMPTY_MAP);
            return response.getStatusCode().equals(HttpStatus.OK);
        }catch (RestClientException e){
            logger.warn("Rout couldn't be added ", e);
        }
        return false;
    }

    @Override
    public boolean unregister(String url) {
        try {
            ResponseEntity<?> response = rest.exchange(FQDN + "/routes?sp=" + url,
                    HttpMethod.POST,null, Object.class, Collections.EMPTY_MAP);
            return response.getStatusCode().equals(HttpStatus.OK);
        }catch (RestClientException e){
            logger.warn("Rout couldn't be deleted, ", e);
        }
        return false;
    }
}
