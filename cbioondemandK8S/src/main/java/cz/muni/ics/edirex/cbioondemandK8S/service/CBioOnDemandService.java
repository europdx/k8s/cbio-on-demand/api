package cz.muni.ics.edirex.cbioondemandK8S.service;

import cz.muni.ics.edirex.cbioondemandK8S.model.InitialBody;
import cz.muni.ics.edirex.cbioondemandK8S.model.Instance;
import cz.muni.ics.edirex.cbioondemandK8S.model.User;

import java.util.List;

public interface CBioOnDemandService {

    /**
     * Creates CbioPortal
     * @param demand which request CbioPortal
     * @return Instance object for handling other requests
     */
    Instance create(InitialBody demand);

    /**
     * Fill status of given instance
     * @param instance instance which status want to be return
     * @return updated Instance object
     */
    Instance status(Instance instance);

    /**
     * Remove given instance,
     * @param instance instance to be removed
     * @return true if instance was removed else false
     */
    boolean remove(Instance instance);

    /**
     * Reschedule task of automatic remove of instance
     * @param instance instance to be reschedule
     * @return updated Instance object
     */
    Instance extend(Instance instance);

    /**
     * Find all instances for given user
     * @param user which want instances
     * @return instance fo user
     */
    List<Instance> listInstancesForUser(User user);
}
