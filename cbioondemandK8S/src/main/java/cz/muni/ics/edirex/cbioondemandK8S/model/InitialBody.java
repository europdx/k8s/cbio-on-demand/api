package cz.muni.ics.edirex.cbioondemandK8S.model;

import java.util.Objects;

public class InitialBody {
    private User user;
    private String sampleIDs;

    public InitialBody() {
    }

    public InitialBody(User user, String sampleIDs) {
        this.user = user;
        this.sampleIDs = sampleIDs;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getSampleIDs() {
        return sampleIDs;
    }

    public void setSampleIDs(String sampleIDs) {
        this.sampleIDs = sampleIDs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InitialBody that = (InitialBody) o;
        return Objects.equals(getUser(), that.getUser()) &&
                Objects.equals(sampleIDs, that.sampleIDs);
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUser(), sampleIDs);
    }
}
