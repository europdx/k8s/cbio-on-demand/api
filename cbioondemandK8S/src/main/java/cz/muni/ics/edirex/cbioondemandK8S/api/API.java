package cz.muni.ics.edirex.cbioondemandK8S.api;

import cz.muni.ics.edirex.cbioondemandK8S.model.InitialBody;
import cz.muni.ics.edirex.cbioondemandK8S.model.Instance;
import cz.muni.ics.edirex.cbioondemandK8S.model.InstanceExtend;
import cz.muni.ics.edirex.cbioondemandK8S.model.InstanceId;
import cz.muni.ics.edirex.cbioondemandK8S.model.User;
import cz.muni.ics.edirex.cbioondemandK8S.service.CBioOnDemandService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@RequestMapping("/v1")
public class API {
    private CBioOnDemandService cBioOnDemand;

    public API(CBioOnDemandService cBioOnDemand) {
        this.cBioOnDemand = cBioOnDemand;
    }

    @PostMapping("/cbioondemand")
    public ResponseEntity createCbioOnDemand(@RequestBody InitialBody demand){
        Instance instance = cBioOnDemand.create(demand);
        if(instance == null)
            return new ResponseEntity<>(HttpStatus.SERVICE_UNAVAILABLE);
        return new ResponseEntity<>(instance , HttpStatus.OK);
    }

    @GetMapping("/cbioondemand")
    public ResponseEntity statusCbioOnDemand(InstanceId id){
        Instance instance = cBioOnDemand.status(id.getInstance());
        if(instance == null)
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>(instance, HttpStatus.OK);

    }

    @DeleteMapping("/cbioondemand")
    public ResponseEntity removeCbioOnDemand(InstanceId id){
        if(!cBioOnDemand.remove(id.getInstance()))
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/cbioondemand")
    public ResponseEntity extendCbioOnDemand(@RequestBody InstanceExtend extend){
        Instance instance =  cBioOnDemand.extend(extend.getInstance());
        if (instance == null)
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity(HttpStatus.OK);
    }


    @GetMapping("/cbioondemands")
    public ResponseEntity listInstancesForUser(User user){
        List<Instance> instances = cBioOnDemand.listInstancesForUser(user);
        if (instances == null){
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(instances, HttpStatus.OK);
    }
}
