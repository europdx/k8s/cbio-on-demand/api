package cz.muni.ics.edirex.cbioondemandK8S.model;

public enum Status {
    creating,
    removing,
    importing,
    failedcreating,
    active,
    notFound,
    decide
}
