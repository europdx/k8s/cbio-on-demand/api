package cz.muni.ics.edirex.cbioondemandK8S.service;

import cz.muni.ics.edirex.cbioondemandK8S.model.IdentifierCRD;
import io.kubernetes.client.models.V1Pod;
import io.kubernetes.client.models.V1ReplicaSet;
import io.kubernetes.client.models.V1Service;
import io.kubernetes.client.models.V1beta1CronJob;

import java.util.List;

public interface KubernetesCalls {

    V1ReplicaSet createReplicaSet(V1ReplicaSet replicaSet, String namespace);
    V1beta1CronJob createCroneJob(V1beta1CronJob cronJob, String namespace);
    V1Service createService(V1Service service, String namespace);
    IdentifierCRD createIdentifier(IdentifierCRD identifier, String namespace);

    boolean deleteReplicaSet(String replicaSetName, String namespace);
    boolean deleteCroneJob(String cronJobName, String namespace);
    boolean deleteService(String serviceName, String namespace);
    boolean deleteIdentifier(String identifierName, String namespace);


    List<V1ReplicaSet> getReplicaSets(String labels, String namespace);
    List<V1Service> getServices(String labels, String namespace);
    List<V1beta1CronJob> getCroneJobs(String labels, String namespace);
    List<V1Pod> getPods(String labels, String namespace);
    IdentifierCRD getIdentifier(String name, String namespace);


    boolean updateCronJob(V1beta1CronJob cronJob, String namespace);
    boolean updateIdentifier(IdentifierCRD identifie, String namespace);
}
