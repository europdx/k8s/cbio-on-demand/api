package cz.muni.ics.edirex.cbioondemandK8S.service;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import cz.muni.ics.edirex.cbioondemandK8S.Utils;
import cz.muni.ics.edirex.cbioondemandK8S.model.IdentifierCRD;
import io.kubernetes.client.ApiClient;
import io.kubernetes.client.ApiException;
import io.kubernetes.client.Configuration;
import io.kubernetes.client.apis.ApiextensionsV1beta1Api;
import io.kubernetes.client.apis.AppsV1Api;
import io.kubernetes.client.apis.BatchV1beta1Api;
import io.kubernetes.client.apis.CoreV1Api;
import io.kubernetes.client.apis.CustomObjectsApi;
import io.kubernetes.client.models.V1DeleteOptions;
import io.kubernetes.client.models.V1Pod;
import io.kubernetes.client.models.V1PodList;
import io.kubernetes.client.models.V1ReplicaSet;
import io.kubernetes.client.models.V1ReplicaSetList;
import io.kubernetes.client.models.V1Service;
import io.kubernetes.client.models.V1ServiceList;
import io.kubernetes.client.models.V1beta1CronJob;
import io.kubernetes.client.models.V1beta1CronJobList;
import io.kubernetes.client.util.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class KubernetesAPI implements KubernetesCalls{
    private CoreV1Api api;
    private AppsV1Api appsApi;
    private BatchV1beta1Api jobApi;

    ApiextensionsV1beta1Api extensionsApi;
    CustomObjectsApi customObjectsApi;

    private static final String PATCH_SCHEDULE = " {\"op\":\"replace\",\"path\":\"/spec/schedule\",\"value\":\"";
    private static final String PATCH_SCHEDULE_END = "\"}";

    private Logger logger = LoggerFactory.getLogger(KubernetesAPI.class);

    public KubernetesAPI() throws IOException {
        ApiClient client = Config.defaultClient();
        Configuration.setDefaultApiClient(client);
        api = new CoreV1Api(client);
        appsApi = new AppsV1Api(client);
        extensionsApi = new ApiextensionsV1beta1Api(client);
        jobApi = new BatchV1beta1Api(client);
        customObjectsApi = new CustomObjectsApi(client);
    }

    @Override
    public V1ReplicaSet createReplicaSet(V1ReplicaSet replicaSet, String namespace){
        try {
            return appsApi.createNamespacedReplicaSet(namespace, replicaSet, false, null,
                    null);
        } catch (ApiException e) {
            logger.warn("ReplicaSet could not be created ");
            logger.warn("Api throw exception while creating ReplicaSet" + "\n " + e.getResponseBody() , e);
        }
        return null;
    }

    @Override
    public V1beta1CronJob createCroneJob(V1beta1CronJob job, String namespace){
        try {
            return jobApi.createNamespacedCronJob(namespace, job, false, null ,null);
        } catch (ApiException e) {
            logger.warn("CronJob could not be created ");
            logger.warn("Api throw exception while creating CronJob" + "\n " + e.getResponseBody() , e);
        }
        return null;
    }

    @Override
    public IdentifierCRD createIdentifier(IdentifierCRD identifierCRD, String namespace){
        try {
            Object o  =  customObjectsApi.createNamespacedCustomObject("example.com","v1beta1", namespace, "identifiers", identifierCRD, null);
            identifierCRD = Utils.convertObjectToIdentifier(o);
            return identifierCRD;
        } catch (ApiException e) {
            logger.warn("IdentifierCRD could not be created");
            logger.warn("Api throw exception while creating CRD" + "\n " + e.getResponseBody() ,e);
        }
        return null;
    }

    @Override
    public V1Service createService(V1Service service, String namespace){
        try {
            return api.createNamespacedService(namespace, service, false, null, null);
        } catch (ApiException e){
            logger.warn("Service could not be created");
            logger.warn("Api throw exception while creating Service" + "\n " + e.getResponseBody() , e);
        }
        return null;
    }

    @Override
    public boolean deleteCroneJob(String cronJobName, String namespace) {
        try {
            jobApi.deleteNamespacedCronJob(cronJobName,namespace,new V1DeleteOptions(), null, null,
                    null, null, null);
            return true;
        } catch (ApiException e) {
            logger.warn("Deleting of job failed");
            logger.info("Api throw exception while deleting job" + "\n " + e.getResponseBody() , e);
        } catch (JsonSyntaxException e){
            logger.info("kubernetes client dependency error ");
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteIdentifier(String identifierName, String namespace) {
        try {
            customObjectsApi.deleteNamespacedCustomObject("example.com","v1beta1", namespace,
                    "identifiers", identifierName, new V1DeleteOptions(), null,
                    null, null);
            return true;
        } catch (ApiException e) {
            logger.warn("Deleting of identifier failed");
            logger.warn("Api throw exception while deleting identifier" + "\n " + e.getResponseBody() , e);
        }
        return false;
    }

    @Override
    public boolean deleteReplicaSet(String name, String namespace){
        try {
            appsApi.deleteNamespacedReplicaSet(name, namespace, new V1DeleteOptions(),
                    null, null, null, null, null);
            return true;
        } catch (ApiException e) {
            logger.warn("ReplicaSet could not be deleted");
            logger.warn("Api throw exception while deleting ReplicaSet" + "\n " + e.getResponseBody() , e);
        }
        return false;
    }


    @Override
    public boolean deleteService(String name , String namespace){
        try {
            api.deleteNamespacedService(name, namespace, new V1DeleteOptions(),
                    null, null, null, null, null);
            return true;
        } catch (ApiException e) {
            logger.warn("Service could not be deleted");
            logger.warn("Api throw exception while deleting Service" + "\n " + e.getResponseBody() , e);
        }
        return false;

    }

    @Override
    public List<V1ReplicaSet> getReplicaSets(String labels, String namespace) {
        try {
            V1ReplicaSetList result = appsApi.listNamespacedReplicaSet(namespace,
                    false, null, null, null,
                    labels, null, null, null, false);
            return result.getItems();
        } catch (ApiException e) {
            logger.warn("Failed to list ReplicaSets");
            logger.warn("Api throw exception while listing ReplicaSets" + "\n" + e.getResponseBody(), e);
        }
        return null;
    }

    @Override
    public List<V1Service> getServices(String labels, String namespace) {
        try {
            V1ServiceList result = api.listNamespacedService(namespace,
                    false, null, null, null,
                    labels, null, null, null, false);
            return result.getItems();
        } catch (ApiException e) {
            logger.warn("Failed to list Service");
            logger.warn("Api throw exception while listing Service" + "\n " + e.getResponseBody() , e);
        }
        return null;
    }

    @Override
    public List<V1beta1CronJob> getCroneJobs(String labels, String namespace) {
        try {
            V1beta1CronJobList result = jobApi.listNamespacedCronJob(namespace,
                    false, null, null, null,
                    labels, null, null, null, false);
            return result.getItems();
        } catch (ApiException e) {
            logger.warn("Failed to list CronJob");
            logger.warn("Api throw exception while listing CronJob", e);
        }
        return null;
    }

    @Override
    public List<V1Pod> getPods(String labels, String namespace) {
        try {
            V1PodList podList = api.listNamespacedPod(namespace,
                    false, null, null, null,
                    labels, null, null, null, false);
            return podList.getItems();
        } catch (ApiException e) {
            logger.warn("Failed to list pods");
            logger.warn("Api throw exception while listing Pods" + "\n " + e.getResponseBody() , e);
        }
        return null;
    }

    @Override
    public IdentifierCRD getIdentifier(String name, String namespace) {
        try {
            return  Utils.convertObjectToIdentifier(customObjectsApi.getNamespacedCustomObject("example.com", "v1beta1", namespace,
                    "identifiers", name));

        } catch (ApiException e) {
            logger.warn("Api throw exception while getting crd" + "\n " + e.getResponseBody() , e);
        }
        return null;
    }

    @Override
    public boolean updateCronJob(V1beta1CronJob cronJob, String namespace) {
        ArrayList<JsonObject> operationToPatch = getOperations(cronJob);
        try {
            jobApi.patchNamespacedCronJob(cronJob.getMetadata().getName(), namespace, operationToPatch, null, null);
            return true;
        } catch (ApiException e) {
            logger.warn("Updating of job failed");
            logger.warn("Api throw exception while patching job" + "\n " + e.getResponseBody() , e);
        }
        return false;
    }

    private ArrayList<JsonObject> getOperations(V1beta1CronJob cronJob) {
        String scheduleCron = cronJob.getSpec().getSchedule();
        JsonElement json = new Gson().fromJson(PATCH_SCHEDULE + scheduleCron + PATCH_SCHEDULE_END, JsonElement.class);
        ArrayList<JsonObject> operationToPatch = new ArrayList<>();
        operationToPatch.add(json.getAsJsonObject());
        return operationToPatch;
    }

    @Override
    public boolean updateIdentifier(IdentifierCRD identifier, String namespace) {
        try {
            customObjectsApi.patchNamespacedCustomObject("example.com","v1beta1", namespace,
                    "identifiers", identifier.getMetadata().getName(), identifier);
            return true;
        } catch (ApiException e) {
            logger.warn("Updating of identifier failed " + e.getResponseBody(), e);
        }
        return false;
    }
}
