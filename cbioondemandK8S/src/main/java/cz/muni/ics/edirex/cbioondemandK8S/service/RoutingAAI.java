package cz.muni.ics.edirex.cbioondemandK8S.service;

public interface RoutingAAI {
    boolean register(String url, String serviceName);
    boolean unregister(String url);
}
