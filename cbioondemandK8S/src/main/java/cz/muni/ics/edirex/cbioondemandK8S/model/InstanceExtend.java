package cz.muni.ics.edirex.cbioondemandK8S.model;

import java.util.Date;
import java.util.Objects;

public class InstanceExtend {
    private String id;
    private User user;
    private long timeToExtend;

    public InstanceExtend(String id, User user, int timeToExtend) {
        this.id = id;
        this.user = user;
        this.timeToExtend = timeToExtend;
    }

    public InstanceExtend() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public long getTimeToExtend() {
        return timeToExtend;
    }

    public void setTimeToExtend(int timeToExtend) {
        this.timeToExtend = timeToExtend;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InstanceExtend that = (InstanceExtend) o;
        return getTimeToExtend() == that.getTimeToExtend() &&
                Objects.equals(getId(), that.getId()) &&
                Objects.equals(getUser(), that.getUser());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getUser(), getTimeToExtend());
    }

    public Instance getInstance(){
        return new Instance(id, user,  timeToExtend , null , null);
    }
}
