package cz.muni.ics.edirex.cbioondemandK8S.service;

import cz.muni.ics.edirex.cbioondemandK8S.Utils;
import cz.muni.ics.edirex.cbioondemandK8S.model.IdentifierCRD;
import cz.muni.ics.edirex.cbioondemandK8S.model.InitialBody;
import cz.muni.ics.edirex.cbioondemandK8S.model.Instance;
import cz.muni.ics.edirex.cbioondemandK8S.model.Status;
import cz.muni.ics.edirex.cbioondemandK8S.model.User;
import io.kubernetes.client.models.V1ContainerStatus;
import io.kubernetes.client.models.V1EnvVar;
import io.kubernetes.client.models.V1Pod;
import io.kubernetes.client.models.V1ReplicaSet;
import io.kubernetes.client.models.V1Service;
import io.kubernetes.client.models.V1beta1CronJob;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Service
public class KubernetesImpl implements CBioOnDemandService {
    public static final int MS_TO_SEC = 1000;
    private final long TIME_TO_LIVE = Utils.HOUR * 4;

    private KubernetesCalls api;
    private RoutingAAI routing;

    private V1Service cbioService;
    private V1Service dbService;
    private V1ReplicaSet cbioReplicaSet;
    private V1ReplicaSet dbReplicaSet;
    private V1beta1CronJob cronJob;
    private IdentifierCRD identifier;

    private final String NAMESPACE;
    private final String APP_LABELS = "app=cbio, type=ondemand";
    private final String DB_LABELS = "app=cbioDB, type=ondemand";

    public KubernetesImpl(KubernetesCalls api, RoutingAAI routing, @Value("${namespace:cbio-on-demand}")String namespace) throws IOException {
        this.api = api;
        this.routing = routing;
        NAMESPACE = namespace;
        cronJob = (V1beta1CronJob) Utils.getConfigurationFromYaml("deletecronjob.yml");
        cbioService = (V1Service) Utils.getConfigurationFromYaml("cbio-service.yml");
        cbioReplicaSet = (V1ReplicaSet) Utils.getConfigurationFromYaml("cbio-replicaset.yml");
        dbReplicaSet = (V1ReplicaSet) Utils.getConfigurationFromYaml("cbiodb-replicaset.yml");
        dbService = (V1Service) Utils.getConfigurationFromYaml("cbiodb-service.yml");
        identifier = Utils.getIdentifierFromYaml("a.yml");
    }

    public Instance create(InitialBody demand){
        User user = demand.getUser();

        IdentifierCRD identifier = generateID();
        if(identifier == null)
            return null;
        String instanceID = identifier.getMetadata().getName();
        String url = generateURL();
        if (!updateIdentifier(identifier, url))
            return null;


        String hostname = createDbService(user.getUserId(), instanceID);
        String cbioService = createCbioService(user.getUserId(), instanceID);

        if(!routing.register(url, cbioService) ||
                hostname == null ||
                cbioService == null ||
                !createInstance(user.getUserId(), instanceID, hostname, url, demand.getSampleIDs())
        ){
            fallBack(hostname, cbioService);
            return null;
        }
        return new Instance(instanceID, user, TIME_TO_LIVE, url, Status.creating);
    }

    private void fallBack(String hostname, String cbioService) {
        if (hostname != null)
            api.deleteService(hostname, NAMESPACE);
        if (cbioService != null)
            api.deleteService(cbioService, NAMESPACE);
    }

    public Instance status(Instance instance){
        String labels = APP_LABELS + ", user=" + instance.getUser().getUserId() + ", instance=" + instance.getId();

        List<V1beta1CronJob> result = api.getCroneJobs(labels, NAMESPACE);
        if (result == null || result.size()!=1)
            return null;
        String cronTime = result.get(0).getSpec().getSchedule();
        long timeToExpire = Utils.getExpireTimeFromCronExpresion(cronTime);


        List<V1Pod> pods = api.getPods(labels, NAMESPACE);
        if(pods.size() != 1){
            return null;
        }
        V1Pod pod = pods.get(0);

        Status status = retrieveStatus(pod);

        if(!statusDecided(status)){
            instance.fillReport(timeToExpire, status);
        }else {
            if (timeToExpire < Utils.FIVE_MINUTES){
                instance.fillReport(timeToExpire, Status.removing);

            }else {
                instance.fillReport(timeToExpire, Status.active);
            }
        }

        String url = getURL(instance.getId());
        instance.setUrl(url);
        return instance;
    }

    public boolean remove(Instance instance){
        boolean rs = deleteReplicaSets(instance);
        boolean svc = deleteServices(instance);
        boolean job = deleteRemoveJob(instance);

        return rs && svc && job;
    }

    public Instance extend(Instance instance){
        String labels = APP_LABELS + ", user=" + instance.getUser().getUserId() + ", instance=" + instance.getId();
        return updateCronjob(instance, labels);
    }

    public List<Instance> listInstancesForUser(User user) {
        String labelsRS = APP_LABELS + ", user=" + user.getUserId();
        List<V1ReplicaSet> result = api.getReplicaSets(labelsRS, NAMESPACE);
        if (result  == null)
            return null;
        if(result.size() == 0)
            return new ArrayList<>();
        List<Instance> returns = new ArrayList<>();

        for (V1ReplicaSet replicaSet : result){
            Map<String, String> labels = replicaSet.getMetadata().getLabels();

            String label = APP_LABELS +", user=" + user.getUserId() + ", instance=" +
                    replicaSet.getMetadata().getLabels().get("instance");
            long expireTime =  getExpireDate(label);
            if (expireTime == -1)
                continue;
            String url = getURL(labels.get("instance"));
            if(url == null)
                continue;
            Instance instance = new Instance(labels.get("instance"), new User().id(labels.get("user")),
                    expireTime , url, null);
            instance = status(instance);
            returns.add(instance);
        }
        return returns;
    }

    private String getURL(String instance) {
        IdentifierCRD identifier = api.getIdentifier(instance, NAMESPACE);
        if (identifier == null)
            return null;
        return identifier.getMetadata().getAnnotations().get("url");
    }


    private String createDbService(String userID, String id) {
        V1Service service = Utils.deepCopy(dbService, V1Service.class);
        addLabelsAndSelectors(service, userID, id);
        service = api.createService(service, NAMESPACE);
        return service.getMetadata().getName();
    }

    private boolean createInstance(String userID, String instanceID, String hostname, String move, String id){
        String db = createDb(userID, instanceID);
        String cbio = createCbio(userID, instanceID, hostname, move, id, move);
        String job = createJob(userID, instanceID);
        if(db == null || cbio == null || job == null){
            fallback(db, cbio, job);
            return false;
        }
        return true;
    }

    private void fallback(String db, String cbio, String job) {
        if (db != null)
            api.deleteReplicaSet(db, NAMESPACE);
        if(cbio != null)
            api.deleteReplicaSet(cbio, NAMESPACE);
        if(job != null)
            api.deleteCroneJob(job, NAMESPACE);
    }


    private String createJob(String userID, String instanceID) {
        V1beta1CronJob job = Utils.deepCopy(cronJob, V1beta1CronJob.class);
        addLabelsAndENV(job, userID, instanceID);
        Date now = new Date();
        setSchedule(job, now.getTime() + TIME_TO_LIVE);
        V1beta1CronJob result = api.createCroneJob(job, NAMESPACE);
        if( result != null)
            return result.getMetadata().getName();
        return null;
    }


    private String createCbio(String userID, String id, String Host, String move, String samplesId, String url ){
        V1ReplicaSet replicaSet = Utils.deepCopy(cbioReplicaSet, V1ReplicaSet.class);
        addLabelsAndSelectors(replicaSet, userID, id);
        addDbHost(replicaSet, Host);
        addDbHostToInit(replicaSet, Host);
        addMoveAndID(replicaSet, move, samplesId);
        addUrl(replicaSet, url);
        V1ReplicaSet result = api.createReplicaSet(replicaSet, NAMESPACE);
        if( result != null)
            return result.getMetadata().getName();
        return null;
    }


    private String createDb(String userID, String id){
        V1ReplicaSet replicaSet = Utils.deepCopy(dbReplicaSet, V1ReplicaSet.class);
        addLabelsAndSelectors(replicaSet, userID, id);
        V1ReplicaSet result = api.createReplicaSet(replicaSet, NAMESPACE);
        if( result != null)
            return result.getMetadata().getName();
        return null;
    }

    private String createCbioService(String userID, String id){
        V1Service service = Utils.deepCopy(cbioService, V1Service.class);
        addLabelsAndSelectors(service, userID, id);
        V1Service result = api.createService(service, NAMESPACE);
        if(result != null)
            return result.getMetadata().getName();
        return null;
    }

    private void addLabelsAndENV(V1beta1CronJob job, String userID, String instanceID) {
        job.getMetadata()
                .putLabelsItem("user", userID)
                .putLabelsItem("instance", instanceID);
        job.getSpec().getJobTemplate().getSpec().getTemplate().getSpec().getContainers().get(0)
                .addEnvItem(new V1EnvVar().name("USER").value(userID))
                .addEnvItem(new V1EnvVar().name("INSTANCE").value(instanceID));
    }

    private void addLabelsAndSelectors(V1Service service, String userID, String id ){
        service.getMetadata()
                .putLabelsItem("user", userID)
                .putLabelsItem("instance", id);
        service.getSpec()
                .putSelectorItem("user", userID)
                .putSelectorItem("instance", id);
    }

    private void addLabelsAndSelectors(V1ReplicaSet replicaSet, String userID, String id ){
        replicaSet.getMetadata()
                .putLabelsItem("user", userID)
                .putLabelsItem("instance", id);

        replicaSet.getSpec().getSelector()
                .putMatchLabelsItem("user", userID)
                .putMatchLabelsItem("instance", id);

        replicaSet.getSpec().getTemplate().getMetadata()
                .putLabelsItem("user", userID)
                .putLabelsItem("instance", id);
    }

    private void addDbHost(V1ReplicaSet replicaSet, String Host){
        replicaSet.getSpec()
                .getTemplate()
                .getSpec()
                .getContainers().get(0)
                .getEnv().get(0)
                .setValue(Host);
    }

    private void addDbHostToInit(V1ReplicaSet replicaSet, String Host){
        replicaSet.getSpec()
                .getTemplate()
                .getSpec()
                .getInitContainers().get(0)
                .getEnv().get(0)
                .setValue(Host);
    }

    private void addUrl(V1ReplicaSet replicaSet, String url) {
        replicaSet.getSpec()
                .getTemplate()
                .getSpec()
                .getContainers().get(0)
                .getEnv().get(4)
                .setValue(url);
    }

    private void addMoveAndID(V1ReplicaSet replicaSet, String move, String samplesId) {
        replicaSet.getSpec()
                .getTemplate()
                .getSpec()
                .getContainers().get(0)
                .getEnv().get(2)
                .setValue(samplesId);

        replicaSet.getSpec()
                .getTemplate()
                .getSpec()
                .getContainers().get(0)
                .getEnv().get(3)
                .setValue(move);
    }

    private void setSchedule(V1beta1CronJob job, long time){
        String cron = Utils.getCronExprresionFromDate(new Date(time));
        job.getSpec().setSchedule(cron);
    }

    private void setSchedule(V1beta1CronJob job, Date date){
        String cron = Utils.getCronExprresionFromDate(date);
        job.getSpec().setSchedule(cron);
    }

    private IdentifierCRD generateID() {
        IdentifierCRD o = Utils.deepCopy(identifier, IdentifierCRD.class);
        return api.createIdentifier(o, NAMESPACE);
    }

    private Status retrieveStatus(V1Pod pod) {
        List<V1ContainerStatus> statuses = pod.getStatus().getContainerStatuses();
        Boolean containerReady = false;
        if (statuses != null)
             containerReady = statuses.get(0).isReady();
        //Pending: The pod has been accepted by the Kubernetes system, but one or more of the container images
        // has not been created. This includes time before being scheduled as well as time spent downloading
        // images over the network, which could take a while. Running: The pod has been bound to a node,
        // and all of the containers have been created. At least one container is still running, or is
        // in the process of starting or restarting. Succeeded: All containers in the pod have terminated
        // in success, and will not be restarted. Failed: All containers in the pod have terminated, and at
        // least one container has terminated in failure. The container either exited with non-zero status or
        // was terminated by the system. Unknown: For some reason the state of the pod could not be obtained,
        // typically due to an error in communicating with the host of the pod.
        String podPhase = pod.getStatus().getPhase();
        Status status = Status.notFound;
        if(containerReady)
            return Status.decide;
        if(podPhase.equals("Pending"))
            return Status.creating;
        if(podPhase.equals("Running"))
            return Status.importing;
        if(podPhase.equals("Failed"))
            return Status.failedcreating;
        return status;
    }

    private long getExpireDate(String labels) {
        List<V1beta1CronJob> result = api.getCroneJobs(labels, NAMESPACE);
        if (result == null || result.isEmpty())
            return -1;
        V1beta1CronJob job = result.get(0);
        return Utils.getExpireTimeFromCronExpresion(job.getSpec().getSchedule());
    }

    private boolean willExpireIn5Minutes(Date timeToExpire, Date nowPlus5Minutes) {
        return nowPlus5Minutes.after(timeToExpire);
    }

    private boolean statusDecided(Status status) {
        return status.equals(Status.decide) || status.equals(Status.active);
    }

    private boolean deleteRemoveJob(Instance instance) {
        String labels = APP_LABELS + ", user=" + instance.getUser().getUserId() + ", instance=" + instance.getId();
        List<V1beta1CronJob> result =api.getCroneJobs(labels, NAMESPACE);
        if(result == null)
            return false;
        if(result.isEmpty())
            return true;
        V1beta1CronJob job = result.get(0);
        return api.deleteCroneJob(job.getMetadata().getName(), NAMESPACE);
    }

    private boolean deleteServices(Instance instance) {
        String labels = APP_LABELS + ", user=" + instance.getUser().getUserId() + ", instance=" + instance.getId();
        boolean cbioService = deleteService(labels);

        String dbLabels = DB_LABELS + ", user=" + instance.getUser().getUserId() + ", instance=" + instance.getId();
        boolean dbService = deleteService(dbLabels);

        return cbioService && dbService;
    }

    private boolean deleteService(String labels) {
        List<V1Service> result = api.getServices(labels, NAMESPACE);
        if(result == null)
            return false;

        if(!result.isEmpty()) {
            V1Service cbioService = result.get(0);
            return api.deleteService(cbioService.getMetadata().getName(), NAMESPACE);
        }
        return true;
    }

    private boolean deleteReplicaSets(Instance instance) {
        String labels = APP_LABELS + ", user=" + instance.getUser().getUserId() + ", instance=" + instance.getId();
        boolean cbioRS = deleteReplicaSet(labels);

        String dbLabels = DB_LABELS + ", user=" + instance.getUser().getUserId() + ", instance=" + instance.getId();
        boolean dbRS = deleteReplicaSet(dbLabels);

        return cbioRS && dbRS;
    }

    private boolean deleteReplicaSet(String labels) {
        List<V1ReplicaSet> result = api.getReplicaSets(labels, NAMESPACE);
        if (result == null)
            return false;

        if (!result.isEmpty()){
            V1ReplicaSet  cbioRS = result.get(0);
            return api.deleteReplicaSet(cbioRS.getMetadata().getName(), NAMESPACE);
        }
        return true;
    }

    private Instance updateCronjob(Instance instance, String labels) {
        List<V1beta1CronJob> result = api.getCroneJobs(labels, NAMESPACE);
        if (result == null)
            return null;

        if (!result.isEmpty()){
            V1beta1CronJob job = result.get(0);

            long expireInSec = Utils.getExpireTimeFromCronExpresion(job.getSpec().getSchedule());
            expireInSec = expireInSec + instance.getSecondsToExpire();
            long newSchedule = new Date().getTime();
            newSchedule += expireInSec * MS_TO_SEC;
            setSchedule(job, newSchedule);
            if(api.updateCronJob(job, NAMESPACE))
                return instance;
        }
        return null;
    }

    private boolean updateIdentifier(IdentifierCRD identifierCRD, String url) {
        identifierCRD.getMetadata().annotations(Collections.singletonMap("url", url));
        return api.updateIdentifier(identifierCRD, NAMESPACE);
    }


    private String generateURL() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }
}
